import os

ENV = os.environ.get('ENVIRONMENT', 'dev')

# This can be class or object
config = {
    'DEBUG': 'True',
    'TESTING': 'False',
    'PROJECT_PATH': os.path.realpath(os.path.dirname(__file__)),
    'ROOT_PATH': os.path.join(os.path.dirname(__file__), '..'),
    'DB_URL': os.getenv('DB_URL', 'postgres://dbuser:dbpass@database:5432/employee'),
    #'DB_URL': os.getenv('DB_URL', 'postgres://dbuser:dbpass@localhost:5432/employee')
}

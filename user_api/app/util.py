from sqlalchemy import (select, case)


def build_response(response, msg, status_code, data=None):
    response['message'], response['status_code'], response['data'] = msg, status_code, data
    return response


def balance_tree(mapper, connection, instance):
    """
    Code taken from
    https://docs.sqlalchemy.org/en/latest/_modules/examples/nested_sets/nested_sets.html

    Balance the Nested Set tree

    :param mapper:
    :param connection:
    :param instance:
    :return:
    """
    # start the left
    if not instance.parent:
        instance.left = 1
        instance.right = 2
    else:
        personnel = mapper.mapped_table
        right_most_sibling = connection.scalar(
            select([personnel.c.rgt]).
                where(personnel.c.emp == instance.parent.emp)
        )

        connection.execute(
            personnel.update(
                personnel.c.rgt >= right_most_sibling).values(
                lft=case(
                    [(personnel.c.lft > right_most_sibling,
                      personnel.c.lft + 2)],
                    else_=personnel.c.lft
                ),
                rgt=case(
                    [(personnel.c.rgt >= right_most_sibling,
                      personnel.c.rgt + 2)],
                    else_=personnel.c.rgt
                )
            )
        )
        instance.left = right_most_sibling
        instance.right = right_most_sibling + 1

        # before_update() would be needed to support moving of nodes
        # after_delete() would be needed to support removal of nodes.

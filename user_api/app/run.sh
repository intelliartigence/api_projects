#!/bin/sh


# wait till postgres is available
until nc -z ${DB_HOST} ${DB_PORT}; do
    echo "$(date) ${DB_HOST} ${DB_PORT} - waiting for postgres..."
    sleep 1
done

export FLASK_APP=app.py
export FLASK_DEBUG=1
flask run -h 0.0.0.0

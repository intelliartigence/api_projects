import logging
from logging.config import fileConfig

from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from . import api
from . import config
from . import models

flask_app = Flask(__name__)

logger = logging.getLogger('app')


def setup():
    """
    Main setup method

    :return:
    """
    setup_logging()
    setup_db()
    setup_flask()


def setup_flask():
    """
    Setup flask related config
    :return:
    """

    flask_app.register_blueprint(api.employee_api)
    ctx = flask_app.app_context()
    ctx.push()


def setup_logging():
    """
    Setup logging

    :return:
    """
    fileConfig('logging_config.ini')


def setup_db():
    """
    Setup database

    :return:
    """
    flask_app.config.update(config.config)
    engine = create_engine(flask_app.config['DB_URL'])
    models.Base.metadata.bind = engine
    logger.info("DB_URL: {}".format(flask_app.config['DB_URL']))
    models.Base.metadata.create_all(engine)
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    flask_app.config['session'] = session


setup()

if __name__ == '__main__':
    flask_app.run(debug=True)
    flask_app.run(port=5000, host='0.0.0.0', threaded=True)

from marshmallow_sqlalchemy import ModelSchema

from . import models


class EmployeeSchema(ModelSchema):
    """
    Employee Schema
    """
    class Meta:
        model = models.Employee


class EmployeeJobSchema(ModelSchema):
    """
    Employee Job Schema
    """
    class Meta:
        model = models.EmployeeJob


emp_schema = EmployeeSchema()
emps_schema = EmployeeSchema(many=True)

emp_job_schema = EmployeeJobSchema()
emp_jobs_schema = EmployeeJobSchema(many=True)

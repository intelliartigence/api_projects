import datetime
import logging

from flask import request, jsonify
from flask import Blueprint


from . import models, schema
from . import util

logger = logging.getLogger()


employee_api = Blueprint('employee_api', __name__)


"""
Employee Endpoints
"""

# endpoint to create new employee
@employee_api.route('/employee', methods=['POST'])
def create_employee():
    response = {}
    if request.method == 'POST':
        input = request.get_json()
        resp = add_employee(input)
        response = util.build_response(response, *resp)

    return jsonify(response)


def add_employee(input):
    msg, status_code = '', ''
    emp = None

    try:
        id = input.get('id')

        if id:
            emp = models.get_session().query(models.Employee).get(id)
            if emp is not None:
                msg, status_code = "models.Employee with id: " + str(emp.id) + " already exists.", 400
                return msg, status_code

        name = input.get('name')
        employee_job = input.get('employee_job')
        new_emp = models.Employee(name)

        if employee_job:
            employee_job_obj = models.EmployeeJob(employee_job)
            new_emp.employee_job = employee_job_obj
            employee_job_obj.employee = new_emp

        models.get_session().add(new_emp)
        models.get_session().commit()
        msg, status_code = "Employee created with id: " + str(new_emp.id) + " successfully!!!", 201

    except Exception as e:
        logger.error(e)
        models.get_session().rollback()
        msg, status_code = "Failed to create Employee with id: " + str(new_emp.id) + ".", 400
    finally:
        return msg, status_code


# endpoint to show all employees
@employee_api.route('/employee', methods=['GET'])
def all_employees():
    response = {}
    try:
        all_emps = models.get_session().query(models.Employee).all()
        output = schema.emps_schema.dump(all_emps)
        response = util.build_response(response, str(len(output.data)) + " employees retrieved successfully!!!", 200,
                                       output.data)
    except Exception as e:
        logger.error(e)
        response = util.build_response(response, "Failed to retrive Employees.", 400)
    finally:
        return jsonify(response)


# endpoint to get employee detail by id
@employee_api.route('/employee/<id>', methods=['GET'])
def retrieve_employee(id):
    response = {}
    try:
        emp = models.get_session().query(models.Employee).get(id)
        if emp is None:
            response = util.build_response(response, "No Employee found for the id: " + str(id), 404)
            return jsonify(response)

        output = schema.emp_schema.dump(emp)
        response = util.build_response(response,
                                       "Employee with id: " + str(emp.id) + " retrieved successfully!!!", 200,
                                       output.data)
    except Exception as e:
        logger.error(e)
        response = util.build_response(response, "Failed to retrive Employee with id: " + emp.id + ".", 404)
    finally:
        return jsonify(response)


# endpoint to update details of employee given in id
@employee_api.route('/employee/<id>', methods=['PUT'])
def update_employee(id):
    response = {}
    try:
        emp = models.get_session().query(models.Employee).get(id)
        if emp is None:
            response = util.build_response(response, "No Employee found for the id: " + str(id), 404)
            return jsonify(response)

        input = request.get_json()
        changed_name = input.get('name')
        employee_job = input.get('employee_job')

        if employee_job:
            employee_job_obj = models.EmployeeJob(employee_job)
            emp.employee_job = employee_job_obj
            employee_job_obj.employee = emp

        supervisor_id = input.get('supervisor_id')
        if changed_name:
            emp.name = changed_name

        if supervisor_id:
            supervisor = models.get_session().query(models.Employee).get(supervisor_id)
            if supervisor:
                emp.supervisor_id = supervisor_id

        models.get_session().add(emp)
        models.get_session().commit()
        response = util.build_response(response, "Employee with id: " + str(emp.id) + " updated successfully!!!",
                                       200)
    except Exception as e:
        logger.error(e)
        models.get_session().rollback()
        response = util.build_response(response, "Failed to update the models.Employee with id: " + str(emp.id) + ".",
                                       400)
    finally:
        return jsonify(response)


# endpoint to delete employee by id
@employee_api.route('/employee/<id>', methods=['DELETE'])
def delete_employee(id):
    response = {}
    try:
        emp = models.get_session().query(models.Employee).get(id)
        if emp is None:
            response = util.build_response(response, "No Employee found for the id: " + str(id), 404)
            return jsonify(response)
        models.get_session().delete(emp)
        models.get_session().commit()
        response = util.build_response(response, "Employee with id: " + id + " deleted successfully!!!", 204)
    except Exception as e:
        logger.error(e)
        response = util.build_response(response, "Failed to delete the Employee with id: " + str(id) + ".", 400)
    finally:
        return jsonify(response)


"""
Employee Job Endpoints
"""

# endpoint to show all employee job by id
@employee_api.route('/employee_job/<id>', methods=['GET'])
def employee_job(id):
    response = {}
    try:
        all_emps = models.get_session().query(models.EmployeeJob).filter_by(employee_id=id)
        output = schema.emp_jobs_schema.dump(all_emps)
        response = util.build_response(response, str(len(output.data)) + " employee jobs retrieved successfully!!!",
                                       200, output.data)
    except Exception as e:
        logger.error(e)
        response = util.build_response(response, "Failed to retrieve employee jobs", 400)
    finally:
        return jsonify(response)


# endpoint to show all employee jobs
@employee_api.route('/employee_jobs', methods=['GET'])
def employee_jobs():
    response = {}
    try:
        all_emps = models.get_session().query(models.EmployeeJob).all()
        output = schema.emp_jobs_schema.dump(all_emps)
        response = util.build_response(response, str(len(output.data)) + " employees jobs retrieved successfully!!!",
                                       200, output.data)
    except Exception as e:
        logger.error(e)
        response = util.build_response(response, "Failed to retrive employees jobs .", 400)
    finally:
        return jsonify(response)


"""
Employee terminate Endpoint
"""

# endpoint to terminate employee by id
@employee_api.route('/employee_terminate/<id>', methods=['GET'])
def terminate_employee(id):
    response = {}
    try:
        emp = models.get_session().query(models.Employee).get(id)
        if emp is None:
            response = util.build_response(response, "No Employee found for the id: " + str(id), 404)
            return jsonify(response)
        emp.active = False
        emp.terminate_date = datetime.datetime.now()
        models.get_session().commit()
        response = util.build_response(response, "Employee with id: " + str(id) + " terminate successfully!!!",
                                       204)
    except Exception as e:
        logger.error(e)
        response['error'], response['status_code'] = "Failed to terminate the Employee with id: " + str(
            id) + ".", 400
    finally:
        return jsonify(response)


"""
Employee Jobs Endpoint
"""


# endpoint to get all the employee bosses
@employee_api.route('/employee_bosses/<id>', methods=['GET'])
def employee_bosses(id):
    response = {}
    try:
        emp = models.get_session().query(models.Employee).get(id)
        if emp is None:
            response = util.build_response(response, "No Employee found for the id: " + str(id), 404)
            return jsonify(response)

        parent_id = emp.supervisor_id
        parents = []

        # DFS for reporting chain
        while parent_id != None:
            boss = models.get_session().query(models.Employee).get(parent_id)
            parents.append(boss.name)
            parent_id = boss.supervisor_id

        response = util.build_response(response, "Reporting chain of employee with id: " + str(id), 204, data=parents)
    except Exception as e:
        logger.error(e)
        response = util.build_response(response, "Failed to Reporting chain of employee with id:: " + str(id) + ".",
                                       400)
    finally:
        return jsonify(response)

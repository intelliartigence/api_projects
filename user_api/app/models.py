import uuid
import datetime

from flask import current_app as flask_app
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy import ForeignKey, Boolean
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Employee(Base):
    """
    Employee Model
    """
    __tablename__ = 'employee'
    id = Column(Integer, primary_key=True)
    unique_id = Column(UUID(as_uuid=True))
    name = Column(String(255), nullable=False)
    start_date = Column(DateTime, default=datetime.datetime.now(), nullable=False)
    terminate_date = Column(DateTime, nullable=True)
    active = Column(Boolean, default=True)
    # parent pointer
    supervisor_id = Column(Integer, nullable=True)
    #employee_jobs = relationship("EmployeeJob")
    employee_job = relationship("EmployeeJob", uselist=False, back_populates="employee")

    # pointers for Nested Set
    left = Column("lft", Integer, nullable=True)
    right = Column("rgt", Integer, nullable=True)

    def __init__(self, name, supervisor_id=None):
        self.name = name
        self.start_date = datetime.datetime.now()
        self.unique_id = uuid.uuid4()
        if supervisor_id:
            self.supervisor_id = supervisor_id

    def __repr__(self):
        return "{}-{}-{}".format(self.id, self.name, self.supervisor_id)


class EmployeeJob(Base):
    """
    Employee Job Model
    """
    __tablename__ = 'employee_job'
    id = Column(Integer, primary_key=True)
    job_title = Column(String(255), nullable=False)
    employee_id = Column(Integer, ForeignKey("employee.id"))
    employee = relationship("Employee", back_populates="employee_job")
    active_date = Column(DateTime, default=datetime.datetime.now(), nullable=False)
    deactive_date = Column(DateTime, nullable=True)

    def __init__(self, job_title):
        self.job_title = job_title

    def __repr__(self):
        return "{}-{}".format(self.id, self.name)


def get_session():
    """
    Get the configured db session

    :return: session
    """
    return flask_app.config['session']

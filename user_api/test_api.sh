#!/usr/bin/env bash
echo

echo
echo "################ Create employee ##################"
echo
curl -X POST -H "Content-Type: application/json" -d '{"name":"John Adams"}' http://localhost:5000/employee
curl -X POST -H "Content-Type: application/json" -d '{"name":"Abe Lincoln"}' http://localhost:5000/employee
curl -X POST -H "Content-Type: application/json" -d '{"name":"Chuck"}' http://localhost:5000/employee


echo
echo "################ Create employee with job title ##################"
echo
curl -X POST -H "Content-Type: application/json" -d '{"name":"Nate", "employee_job":"Engineer"}' http://localhost:5000/employee


echo
echo "################ Update employee , assign suerpvisor ##################"
echo
curl -X PUT -H "Content-Type: application/json" -d '{"supervisor_id":"1"}' http://localhost:5000/employee/2
curl -X PUT -H "Content-Type: application/json" -d '{"supervisor_id":"2"}' http://localhost:5000/employee/3

echo
echo "################ Update employee, change job title ##################"
echo
curl -X PUT -H "Content-Type: application/json" -d '{"employee_job":"CEO"}' http://localhost:5000/employee/4
curl -X PUT -H "Content-Type: application/json" -d '{"employee_job":"Director"}' http://localhost:5000/employee/4
curl -X PUT -H "Content-Type: application/json" -d '{"employee_job":"Board Member"}' http://localhost:5000/employee/4


echo "################## Fetch the details of the employee created earlier from database ####################"
echo
curl localhost:5000/employee/1


echo
echo "################# Terminate employee ####################"
echo
curl -X "GET" localhost:5000/employee_terminate/4

#echo
#echo "################# Delete employee ####################"
#echo
#curl -X "DELETE" localhost:5000/employee/8

echo
echo "################# Reporting Chain of Employee 3 ####################"
echo
curl -X "GET" localhost:5000/employee_bosses/3

echo
echo "################# Employee Jobs table ####################"
echo
curl localhost:5000/employee_jobs

echo
echo "################ Fetch all employees in the database ###############"
echo
curl localhost:5000/employee


Employee Relations API


### Run
1. type ``` make up ```
2. type ```./test_client``` for basic api usercase tests


### Features and Design

1. Based on Flask , Postgres, Sql-alchemy
2. End-Point for Adding Employee, Updating employee boss and job title
3. Self relations DFS for reporting chain with explanation of Nested Sets
4. Check API tests in ```./test_api.sh```


### Tests
1. See ```./test_api.sh``` for api tests for use-cases


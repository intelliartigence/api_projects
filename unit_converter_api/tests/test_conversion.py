from unittest import TestCase

from hamcrest import assert_that, equal_to, raises, calling

from converter import Lexer, Evaluator, Transformer, TokenType, \
    Token, SIUnit, common_units_to_si_units


class TestMainFunction(TestCase):
    def test_valid(self):
        input_string = "(degree/minute)"
        expected = {'unit_name': '(rad/s)', 'multiplication_factor': '0.00029088820867'}
        response = common_units_to_si_units(input_string)
        assert_that(response, equal_to(expected))

    def test_valid_symbols(self):
        input_string = "ha*°"
        expected = {'unit_name': 'm2*rad', 'multiplication_factor': '174.53292519943295'}
        response = common_units_to_si_units(input_string)
        assert_that(response, equal_to(expected))

    def test_invalid_token(self):
        input_string = "(degree/minutecule)"
        assert_that(calling(common_units_to_si_units).with_args(input_string), raises(Exception))


class TestLexer(TestCase):
    def test_valid(self):
        input_string = "(degree/minute)"

        expected = [Token('(', TokenType.LP),
                    Token('degree', TokenType.UNIT),
                    Token('/', TokenType.DIVIDE),
                    Token('minute', TokenType.UNIT),
                    Token(')', TokenType.RP)]

        lexer = Lexer()
        lexer.offer(input_string)
        tokens = [x for x in lexer.token()]
        assert_that(tokens, equal_to(expected))


class TestTrannsformer(TestCase):
    def test_valid(self):
        tokens = [Token('(', TokenType.LP),
                  Token('degree', TokenType.UNIT),
                  Token('/', TokenType.DIVIDE),
                  Token('minute', TokenType.UNIT),
                  Token(')', TokenType.RP)]

        expected = [Token('(', TokenType.LP),
                    SIUnit('degree', TokenType.UNIT, 'rad', 0.017453292519943295),
                    Token('/', TokenType.DIVIDE),
                    SIUnit('minute', TokenType.UNIT, 's', 60),
                    Token(')', TokenType.RP)]

        transformer = Transformer()
        si_tokens = [transformer.convert(x) if x.type is TokenType.UNIT else x for x in tokens]
        assert_that(si_tokens, equal_to(expected))


class TestEvaluator(TestCase):
    def test_valid(self):
        si_tokens = [Token('(', TokenType.LP),
                     SIUnit('degree', TokenType.UNIT, 'rad', 0.017453292519943295),
                     Token('/', TokenType.DIVIDE),
                     SIUnit('minute', TokenType.UNIT, 's', 60),
                     Token(')', TokenType.RP)]

        expected = [SIUnit('degree', TokenType.UNIT, 'rad', 0.017453292519943295),
                    SIUnit('minute', TokenType.UNIT, 's', 60),
                    Token('/', TokenType.DIVIDE)]

        expected_multiplication_factor = 0.0002908882086657216

        evaluator = Evaluator()
        postfix_si_tokens = evaluator.infix_to_post_fix(si_tokens)
        multiplication_factor = evaluator.solve()

        assert_that(multiplication_factor, equal_to(expected_multiplication_factor))
        assert_that(postfix_si_tokens, equal_to(expected))

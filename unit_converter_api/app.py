# -*- coding: utf-8 -*-

import logging
from flask import Blueprint
from flask import Flask
from flask import jsonify
from flask import request
from logging.config import fileConfig

from converter import common_units_to_si_units

logger = logging.getLogger()

# flask app & api
flask_app = Flask(__name__)
units_api = Blueprint('units_api', __name__)


# units endpoint
@units_api.route('/units/si', methods=['GET'])
def convert_units():
    units = request.args.get('units')
    response = {}

    if not units:
        response['message'], response['status_code'] = "Missing units", 400
        return jsonify(response)

    logger.info("units: {}".format(units))

    try:

        # main facade method that hides the inner classes
        response = common_units_to_si_units(units)

    except Exception as e:

        # any exception, return 500 and exception message
        logger.error(e)
        response['message'], response['status_code'] = str(e), 500

    finally:
        return jsonify(response)


def setup_app():
    """
    flask app setup method
    sets up logging and api blueprint for flask

    :return:
    """
    fileConfig('logging_config.ini')
    flask_app.register_blueprint(units_api)


# setup the logging & flask
setup_app()

if __name__ == '__main__':
    flask_app.run(debug=True)
    flask_app.run(port=5000, host='0.0.0.0', threaded=True)

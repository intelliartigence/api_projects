#!/usr/bin/env bash
echo

echo
echo "################# SI Conversion API tests  ####################"
echo
printf "\nunits: degree\n"
curl -X "GET" "localhost:5000/units/si?units=degree"

printf "\nunits: degree/minute\n"
curl -X "GET" "localhost:5000/units/si?units=degree/minute"

printf "\nunits: (degree/minute)\n"
curl -X "GET" "localhost:5000/units/si?units=(degree/minute)"

printf "\nunits: (degree/(minute*hectare))\n"
curl -X "GET" "localhost:5000/units/si?units=(degree/(minute*hectare))"

printf "\nunits: ha*min\n"
curl -X "GET" "localhost:5000/units/si?units=ha*min"

printf "\nunits: degree/'\n"
curl -X "GET" "localhost:5000/units/si?units=(degree/')"

printf '\nunits: degree/"\n'
curl -X "GET" 'localhost:5000/units/si?units=(degree/")'

printf "\nunits: ha*degree\n"
curl -X "GET" "localhost:5000/units/si?units=ha*degree"

printf "\nunits: ha*°\n"
curl -X "GET" -G "localhost:5000/units/si" --data-urlencode "units=ha*°"

printf "\nunits: ha*degreeeeeeee\n"
curl -X "GET" "localhost:5000/units/si?units=ha*degreeeeeeee"

printf "\nunits: \n"
curl -X "GET" "localhost:5000/units/si"

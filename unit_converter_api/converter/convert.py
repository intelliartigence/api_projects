from converter import Lexer, Evaluator, Transformer, TokenType, check_input_units


def common_units_to_si_units(input_string):
    """
    Main conversion facade function

    1. Creates lexer/tokenizer
    2. Get tokens from lexer
    3. creates si_units by transforming tokens to si units/conversion_factors
    4. creates evaluator to convert si_token from infix to post_fix notation
    5. calculates the multiplication_factor by multiply/division of postfix notations

    :param input_string: input string
    :return: conversion_object
    """

    # Tokenize the input based on our input rules
    lexer = Lexer()
    lexer.offer(input_string)
    tokens = [x for x in lexer.token()]

    # if un-recognized token, raise Exception
    if not check_input_units(tokens):
        raise Exception('Invalid token')

    # Transform the 'widely used' units in tokens to SI
    transformer = Transformer()
    si_tokens = [transformer.convert(x) if x.type is TokenType.UNIT else x for x in tokens]

    # Convert infix -> postfix via shunting-yard algo and solve for 'conversion_factor'
    evaluator = Evaluator()
    evaluator.infix_to_post_fix(si_tokens)
    multiplication_factor = evaluator.solve()

    # Limit to 14 significant digits
    multiplication_factor = "{0:.14f}".format(multiplication_factor)

    # get unit_name by joining si_transformed units
    unit_name = ''.join([x.si_counterpart if x.type is TokenType.UNIT else x.name for x in si_tokens])

    # conversion_object dict
    return {
        "unit_name": unit_name,
        "multiplication_factor": multiplication_factor
    }

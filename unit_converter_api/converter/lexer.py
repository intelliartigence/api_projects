import logging
import re

from .token_utils import TokenType, Token

logger = logging.getLogger()

# Regex Rules to parse the input
REGEX_RULES = [
    # a-zA-Z alphabet characters, operators ', " and this thing °

    (r'[a-zA-Z°\'"]+', TokenType.UNIT.name),
    (r'\*', TokenType.MULTIPLY.name),
    (r'\/', TokenType.DIVIDE.name),
    (r'\(', TokenType.LP.name),
    (r'\)', TokenType.RP.name),
]


class Lexer(object):
    """
    Regex based Lexer/Tokenizer

    1. Uses a set of regex rules
    2. creates named-groups regex set, name of group is name of token
    3. combines regex_set into single regex with OR operator '|'
    4. sets the input via offer() function
    5. run the regex from start of input via input_idx,
        if a match is found, token is yeilded from this iterator
        and position to check next token advances by updating input_idx


    """

    def __init__(self, regex_rules=REGEX_RULES):

        # position to check the match
        self.input_idx = 0

        # input buffer
        self.input_buffer = None

        regex_rules = regex_rules

        # add a named-group for each type of regex
        # name of group is TokenType.name (defined in REGEX_RULES)
        group_regex_rules = []
        for regex, rtype in regex_rules:
            # group-name is same as token type
            group_name = '%s' % rtype

            # create named-group, same as django url pattern
            group_regex_rules.append('(?P<%s>%s)' % (group_name, regex))

        # compile the list of group_regexes into single regex with OR operator |
        self.full_regex = re.compile('|'.join(group_regex_rules))

    def offer(self, value):
        """
        Input offered to Lexer

        :param value: input value
        :return: None
        """

        # reset the index and buffer if new input is added
        self.input_idx = 0
        self.input_buffer = value

    def token(self):
        """
        Iterator function, keeps returning while token exists

        scales linearly on input size, it is stream oriented as chunks of
        input can be offered to it via offer() function

        :return: parsed Token object or None
        """

        # if we reached end, return None
        if self.input_idx >= len(self.input_buffer):
            return None

        # go through the input string
        # match at first input_idx
        # if match found , yield token
        # advance index to next token by updating input_idx

        # while there are characters/tokens in input
        while self.input_idx < len(self.input_buffer):

            # match only starting at input_idx
            match = self.full_regex.match(self.input_buffer, self.input_idx)

            if match:
                name = match.group()  # name of token
                group_name = match.lastgroup  # name of named-group i.e. token-type
                token_type = TokenType(group_name)  # make enum
                self.input_idx = match.end()  # advance the index to next position
                token = Token(name, token_type)

                logger.info("Token: name:{} group_name:{} token_type:{} input_idx: {}".
                            format(name, group_name, token_type, self.input_idx))


                # yield to make it iterable, this function is a generator function
                yield token

            else:
                # the input at this index did not match any rule
                # i.e. input token is invalid and not supported
                raise Exception(f"Invalid input token at {self.input_idx}")

from math import pi

from .token_utils import SIUnit

# Lookup Table for SI counterparts
SI_CONVERSION_LOOKUP = {
    "minute": {"counterpart": "s", "conversion_factor": 60},
    "hour": {"counterpart": "s", "conversion_factor": 3600},
    "day": {"counterpart": "s", "conversion_factor": 86400},
    "degree": {"counterpart": "rad", "conversion_factor": pi / 180},
    "arcminute": {"counterpart": "rad", "conversion_factor": pi / 10800},
    "arcsecond": {"counterpart": "rad", "conversion_factor": pi / 648000},
    "hectare": {"counterpart": "m2", "conversion_factor": 10000},
    "litre": {"counterpart": "m3", "conversion_factor": 0.001},
    "tonne": {"counterpart": "kg", "conversion_factor": 1000},

    # abbreviations
    "min": {"counterpart": "s", "conversion_factor": 60},
    "h": {"counterpart": "s", "conversion_factor": 3600},
    "d": {"counterpart": "s", "conversion_factor": 86400},
    "°": {"counterpart": "rad", "conversion_factor": pi / 180},
    "'": {"counterpart": "rad", "conversion_factor": pi / 10800},
    '"': {"counterpart": "rad", "conversion_factor": pi / 648000},
    "ha": {"counterpart": "m2", "conversion_factor": 10000},
    "L": {"counterpart": "m3", "conversion_factor": 0.001},
    "t": {"counterpart": "kg", "conversion_factor": 1000},

}

# module level list of acceptable tokens
acceptable_input_units = list(SI_CONVERSION_LOOKUP.keys())
acceptable_input_units.extend(['(', ')', '*', '/'])


def check_input_units(tokens):
    """
    Checks if tokens are acceptable

    :param tokens: parsed tokens
    :return: Boolean
    """
    for token in tokens:
        if token.name not in acceptable_input_units:
            return False

    return True


class Transformer(object):
    """
    Transforms Token to SIUnit by using lookup table
    """

    def __init__(self, lookup_table=SI_CONVERSION_LOOKUP):
        self.lookup_table = lookup_table

    def convert(self, token):
        """
        Convert Token to SI Unit adding conversion_factor

        :param token: parsed token
        :return: si unit object
        """
        si_token_items = self.lookup_table[token.name]
        si_unit = SIUnit(token.name,
                         token.type,
                         si_token_items["counterpart"],
                         si_token_items["conversion_factor"])
        return si_unit

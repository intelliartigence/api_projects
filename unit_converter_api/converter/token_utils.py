from enum import Enum


class TokenType(Enum):
    """
    Token types
    """

    LP = "LP"
    RP = "RP"
    UNIT = "UNIT"
    MULTIPLY = "MULTIPLY"
    DIVIDE = "DIVIDE"


# Operators that are supported
operators = [TokenType.DIVIDE, TokenType.MULTIPLY]


class Token(object):
    """
    Token object

    created after first lexical analysis by lexer
    """

    def __init__(self, name, type):
        self.name = name
        self.type = type

    def __eq__(self, other):
        return self.name == other.name and self.type == other.type

    def __repr__(self):
        return 'Token(%s,%s)' % (self.name, self.type)


class SIUnit(Token):
    """
    SIUnit, created after Token -> SIUnit transformation

    inherits from Token for common fields
    """

    def __init__(self, name, type, si_counterpart, si_conversion_factor):
        super(SIUnit, self).__init__(name, type)
        self.si_counterpart = si_counterpart
        self.si_conversion_factor = si_conversion_factor

    def __eq__(self, other):
        return self.name == other.name \
               and self.type == other.type \
               and self.si_counterpart == other.si_counterpart \
               and self.si_conversion_factor == other.si_conversion_factor

    def __repr__(self):
        return 'SIUnit(%s,%s,%s,%s)' % (self.name,
                                        self.type,
                                        self.si_counterpart,
                                        self.si_conversion_factor)


def is_unit(si_unit):
    """
    Helper function

    :param si_unit: si_unit object
    :return: is si_unit of type UNIT and not operator
    """
    return si_unit.type is TokenType.UNIT

import logging
from queue import Queue

from .token_utils import is_unit, operators, TokenType

logger = logging.getLogger()


class Evaluator(object):
    """
    Convert postfix to infix

    using simplified shunting yard algorithm
    evaluate the resulting expression by solving conversion_factors

    """

    def __init__(self):

        # Stack top
        self.stack_top = -1

        # stacks for internal use
        # output_stack is used in infix -> postfix
        # result_stack is used in evaluation of postfix
        self.output_stack = None
        self.result_stack = []

    def multiply(self):
        x, y = self.result_stack.pop(), self.result_stack.pop()
        self.result_stack.append(x * y)

    def divide(self):
        x, y = self.result_stack.pop(), self.result_stack.pop()
        self.result_stack.append(y / x)

    def infix_to_post_fix(self, tokens):
        """
        Simplified shunting yard algorithm
        to convert infix to postfix notation

        Shunting yard algorithm:
        https://en.wikipedia.org/wiki/Shunting-yard_algorithm

        It linear-izes infix (infix tree) to postfix and arranges postfix
        operand/operators in a list to be evaluated in serial-fashion in
        stack-based machines

        1. scan tokens, if operand put on output-queue
        2. else if operator, pop existing  (till L-paran) from stack
            and add to output-queue
        3. put operator to queue
        4. else if left-paranthesis, push to stack
        5. else if r-paran, pop existing  (till L-paran) from stack
            and add to output-queue, pop l-parn
        6. drain stack and add to output-queue

        :param tokens: inflix token list
        :return: postfix token list
        """

        # local queue and stack
        queue = Queue()
        stack = []

        for token in tokens:

            # if it is operand
            if is_unit(token):
                queue.put(token)

            # if it is operator
            elif token.type in operators:

                # if new operator pop operators from stack (till left-parenthesis)
                # and append to queue
                while (
                        # stack is not empty and
                        stack and
                        # and the operator at the top of the operator stack is not a left parenthesis
                        stack[self.stack_top] != TokenType.LP and stack[self.stack_top] in operators
                ):
                    queue.put(stack.pop())

                # add operator to stack (re-init stack)
                stack.append(token)

            elif token == TokenType.LP:

                # left-parenthesis is operator
                stack.append(token)

            elif token == TokenType.RP:

                # find immediate left-parenthesis and pop all items and put them in queue
                while (
                        # stack is not empty and
                        stack and
                        # and the operator at the top of the operator stack is not a left parenthesis
                        stack[self.stack_top] != TokenType.LP
                ):
                    queue.put(stack.pop())

                # if left-parenthesis is found for this right-parenthesis, drop it
                if stack and stack[self.stack_top] == TokenType.LP:
                    stack.pop()

        # drain the stack into queue
        while stack:
            queue.put(stack.pop())

        # keep the post_fix tokens, to be used in solve() method
        self.output_stack = list(queue.queue)
        return self.output_stack

    def solve(self):
        """
        Solve the postfix with result_stack

        1. if operand(unit), push to stack
        2. if operator (mul/div), pop 2 items, evaluate operator
        3. push result to stack from #2

        :return:
        """

        for token in self.output_stack:
            logger.info("token:{} name:{} ".format(token, token.type.name.lower()))

            if token.type is TokenType.UNIT:
                # append the numerical value to facilitate the computation
                self.result_stack.append(token.si_conversion_factor)

            elif token.type in operators:
                try:
                    # if operator, get corresponding divide/multiply method
                    # from this class
                    method = getattr(self, token.type.name.lower())
                    logger.info("method {}".format( method))

                    # execute the method, resulting in poping the operands and
                    # computing the expression
                    method()
                except AttributeError as e:
                    logger.info('Invalid Operator {} {} {}'.format( self.output_stack, self.result_stack, str(e)))

        return self.result_stack.pop()

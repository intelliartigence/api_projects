### Task:
Create a web-service that converts widely-used units to their SI counterparts, with
multiplication conversion factor

### Approach:
1. Parse the input based to generate tokens using simple lexical analysis
2. Convert tokens to their SI counterparts
3. Use Shunting yard algorithm to convert infix SI Tokens to postix
4. Solve the postfix expression

### How to Run:
1. Check that python3, Docker and make are installed on your machine
2. type `cd unit_converter_api/unit_converter`
3. type `make up` to build/run docker

### Running Test:
1. create virtual environment and install dependancies by typing 
    `pip install -r requirements.txt`
2. Unit tests are in tests/ folder, To run them type `pytest .` or `make test` 
    in unit_converter folder
3. API tests are in `test_api.sh`, type `.test_api.sh` or `make test_api` 

### Make file, Logging
1. A make file with basic docker commands is added to make the startup easy
2. You can use following make commands 
    - make up 
    - make down
    - make logs
    - make test
    - make test_api
    - etc
    

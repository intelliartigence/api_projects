package com.comment.utils;

/**
 * SortType Enum of RATING & DATE
 */
public enum SortType {
    RATING("rating"),
    DATE("date"),
    NONE("none");

    private String type;

    SortType(String type) {
        this.type = type;
    }
}

package com.comment.utils;

/**
 * SortStrategy of different implementation with tradeoffs
 * <p>
 * I did not get time to implement all, so only SORTED_IN_MEMORY_TREE is implemented
 */
public enum SortStrategy {
    SORTED_IN_MEMORY_TREE("sorted.in.mem.tree"),
    UNSORTED_IN_MEMORY_TREE("unorted.in.mem.tree"),
    BUILD_TREE_AT_REQUEST("tree.at.request");

    private String type;

    SortStrategy(String type) {
        this.type = type;
    }
}

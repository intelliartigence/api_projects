package com.comment.services.tree;

import com.comment.model.Comment;
import com.comment.utils.SortType;

import java.util.*;

public class CommentNode {

    private Comment comment;

    // Map of sort type --> List of sorted children
    private Map<SortType, List<CommentNode>> children = new HashMap<SortType, List<CommentNode>>();

    public CommentNode() {
    }

    public CommentNode(Comment comment) {
        this.comment = comment;
    }


    /**
     * Add given list as children of type sortType
     *
     * @param sortType
     * @param childNodes
     */
    public void addChildren(SortType sortType, List<CommentNode> childNodes) {

        List<CommentNode> children = this.children.get(sortType);
        if (children == null) {
            children = new ArrayList<CommentNode>();
            this.children.put(sortType, children);
        }

        // As each list of children is added, it is sorted
        sortChildren(sortType, childNodes);
        children.addAll(childNodes);
    }

    /**
     * Sort the children list based on sortType
     *
     * @param sortType
     * @param childNodes
     */
    public void sortChildren(SortType sortType, List<CommentNode> childNodes) {
        Comparator comparator = SortComparator.getComparator(sortType);
        Collections.sort(childNodes, comparator);
    }

    /**
     * Get Associated Comment
     *
     * @return
     */
    public Comment getComment() {
        return comment;
    }

    /**
     * Return Boolean.TRUE if contains children
     *
     * @param sortType
     * @return
     */
    public Boolean hasChildren(SortType sortType) {
        return this.children.size() > 0;
    }

    /**
     * Get Childrens
     *
     * @param sortType
     * @return
     */
    public List<CommentNode> getChildren(SortType sortType) {
        List<CommentNode> nodes = this.children.get(sortType);

        if (nodes == null) {
            nodes = new ArrayList<CommentNode>();
            this.children.put(sortType, nodes);
        }
        return nodes;
    }

    /**
     * Recursively Get Children of given sortType
     *
     * @param results
     * @param commentNode
     */
    void getParentChildsRecursive(SortType sortType, List<Comment> results, CommentNode commentNode) {

        results.add(commentNode.getComment());

        if (commentNode.hasChildren(sortType)) {

            for (CommentNode childNode : commentNode.getChildren(sortType))
                this.getParentChildsRecursive(sortType, results, childNode);
        }

    }

    /**
     * Get All the children in given order
     *
     * @param sortType
     * @return
     */
    public List<Comment> getParentChilds(SortType sortType) {

        List<Comment> allComments = new ArrayList<Comment>();
        this.getParentChildsRecursive(sortType, allComments, this);// pass parent i.e. root of this subtree
        return allComments;

    }
}

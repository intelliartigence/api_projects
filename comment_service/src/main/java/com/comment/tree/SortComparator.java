package com.comment.services.tree;

import com.comment.utils.SortType;

import java.util.Comparator;

public class SortComparator {

    /**
     * Factory Method to get Comparator based on sortType
     *
     * @param sortType
     * @return
     */
    public static Comparator<CommentNode> getComparator(SortType sortType) {

        if (sortType == SortType.RATING) return RatingsComparator;
        else if (sortType == SortType.DATE) return TimeStampComparator;
        else return null;

    }

    // Ratings Comparator
    public static Comparator<CommentNode> RatingsComparator = new Comparator<CommentNode>() {
        public int compare(CommentNode comment, CommentNode comment2) {
            return comment2.getComment().getRating().compareTo(comment.getComment().getRating());
        }
    };


    // Date/Timestamp Comparator
    public static Comparator<CommentNode> TimeStampComparator = new Comparator<CommentNode>() {
        public int compare(CommentNode comment, CommentNode comment2) {
            return comment2.getComment().getTimeStamp().compareTo(comment.getComment().getTimeStamp());
        }
    };

}

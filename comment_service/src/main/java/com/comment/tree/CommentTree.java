package com.comment.services.tree;

import com.comment.utils.SortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CommentTree {

    private Logger log = LoggerFactory.getLogger(CommentTree.class);

    // Eager Singleton
    private static final CommentTree instance = new CommentTree();

    private CommentTree() {
    }

    public static CommentTree getInstance() {
        return instance;
    }

    // Map of sort type --> Node for root elements
    private Map<SortType, List<CommentNode>> rootLevelComments = new HashMap<SortType, List<CommentNode>>();

    /**
     * Add root nodes to tree
     *
     * @param sortType
     * @param commentNodes
     */
    public void addRootLevelNodes(SortType sortType, List<CommentNode> commentNodes) {

        List<CommentNode> children = this.rootLevelComments.get(sortType);
        if (children == null) {
            children = new ArrayList<CommentNode>();
            this.rootLevelComments.put(sortType, children);
        }

        Comparator comparator = SortComparator.getComparator(sortType);
        if (comparator != null)
            Collections.sort(commentNodes, comparator);
        children.addAll(commentNodes);
    }


    /**
     * Get Root Level Comments
     *
     * @param sortType
     * @return
     */
    public List<CommentNode> getRootLevelComments(SortType sortType) {
        List<CommentNode> nodes = this.rootLevelComments.get(sortType);
        if (nodes == null) {
            nodes = new ArrayList<CommentNode>();
            this.rootLevelComments.put(sortType, nodes);
        }
        return nodes;
    }

    /**
     * Build the In-Memory Tree Recursively
     *
     * @param sortType
     * @param commentNode
     * @param parentChildMap
     */
    private void buildSortTreeRecursive(SortType sortType, CommentNode commentNode, Map<Long, List<CommentNode>> parentChildMap) {

        List<CommentNode> childNodes = parentChildMap.get(commentNode.getComment().getCommentId());

        if (childNodes != null)
            commentNode.addChildren(sortType, childNodes);

        for (CommentNode childNode : commentNode.getChildren(sortType)) {
            this.buildSortTreeRecursive(sortType, childNode, parentChildMap);

        }
    }

    /**
     * Build Tree from given Comment list , of type sortType
     *
     * @param clist
     * @param sortType
     */
    public void buildSortTree(List<com.comment.model.Comment> clist, SortType sortType) {

        log.debug("buildSortTree " + clist);

        // Build ParentId-->Children Lookup Map
        Map<Long, List<CommentNode>> parentChildMap = new HashMap<Long, List<CommentNode>>();
        for (com.comment.model.Comment comment : clist) {

            List<CommentNode> childrens = parentChildMap.get(comment.getParentId());
            if (childrens == null) {
                childrens = new ArrayList<CommentNode>();
                parentChildMap.put(comment.getParentId(), childrens);
            }
            childrens.add(new CommentNode(comment));
        }

        // Add root nodes
        List<CommentNode> rootNodes = parentChildMap.get(new Long(0));
        CommentTree.getInstance().addRootLevelNodes(sortType, rootNodes);

        List<CommentNode> rootComments = CommentTree.getInstance().getRootLevelComments(sortType);
        log.debug("topComments " + rootComments.size());

        // DFS for each node to build children list
        for (CommentNode commentNode : rootComments) {
            buildSortTreeRecursive(sortType, commentNode, parentChildMap);
        }
    }

}


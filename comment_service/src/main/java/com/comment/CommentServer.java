package com.comment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * Main Spring Boot Applicate
 */
@Configuration
@ComponentScan
@SpringBootApplication
@EnableAutoConfiguration
public class CommentServer {

    public static void main(String[] args) {
        SpringApplication.run(CommentServer.class, args);
    }

}

package com.comment.model;

import javax.persistence.*;
import java.io.Serializable;


/**
 * Comment Model:
 * This JPA entity and model for Comment with proeprties commentId, comment, parent_commentId , rating, date
 */
@Entity
@Table(name = "comments")
public class Comment implements Serializable {

    public Comment() {
    }

    public Comment(long commentId, long parentId, long timeStamp, long rating, String comment) {
        this.commentId = commentId;
        this.parentId = parentId;
        this.timeStamp = timeStamp;
        this.rating = rating;
        this.comment = comment;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long commentId;


    @Column
    private String comment;


    @Column
    private Long rating;


    //@Column(columnDefinition = "DATE DEFAULT CURRENT_DATE")
    //@JsonFormat(pattern="yyyy-MM-dd")
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    //@Temporal(TemporalType.TIMESTAMP)
    //private Date timeStamp;
    @Column
    private Long timeStamp;

    @Column
    private Long parentId;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", comment='" + comment + '\'' +
                ", rating=" + rating +
                ", timeStamp=" + timeStamp +
                ", parentId=" + parentId +
                '}';
    }
}
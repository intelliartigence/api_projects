package com.comment.controller;

import com.comment.model.Comment;
import com.comment.services.CommentService;
import com.comment.utils.SortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest API Implementation using HTTP Methods GET for comments
 */
@RestController
@RequestMapping("/api/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    private Logger log = LoggerFactory.getLogger(CommentController.class);


    // Get Sorted  comments
    @RequestMapping(method = RequestMethod.GET)
    public List<Comment> getSortedComemnts(@RequestParam(value = "sort", required = true) String sort) {

        // Some parameter checks
        if (sort == null || sort.isEmpty()) {
            throw new IllegalArgumentException("The 'sort' parameter must not be null or empty");
        }

        log.debug("sort: " + sort + "---" + SortType.DATE.name() + SortType.RATING.name());

        if (!sort.equalsIgnoreCase(SortType.RATING.name()) && !sort.equalsIgnoreCase(SortType.DATE.name())) {
            throw new IllegalArgumentException("Invalid 'sort' parameter, use valid sort value");
        }

        SortType sortType = SortType.valueOf(sort);
        return commentService.getSortedComemnts(sortType);

    }

}

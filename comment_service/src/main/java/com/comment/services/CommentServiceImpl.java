package com.comment.services;

import com.comment.model.Comment;
import com.comment.repositories.CommentRepository;
import com.comment.services.tree.CommentNode;
import com.comment.services.tree.CommentTree;
import com.comment.utils.SortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Comment Service Implementation
 */
@Service
public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;

    private Logger log = LoggerFactory.getLogger(CommentServiceImpl.class);

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    /**
     * Get Comments from DB of sortType
     *
     * @param sortType
     * @return
     */
    @Override
    public List<Comment> getCommentsFromDB(SortType sortType) {

        log.info("sortType " + sortType);

        List<Comment> clist = new ArrayList<Comment>();

        for (Comment Comment : commentRepository.findAll()) {
            clist.add(Comment);
        }
        return clist;
    }

    /**
     * Save Comment in DB
     *
     * @param comment
     * @return
     */
    @Override
    public Comment saveComment(Comment comment) {
        return commentRepository.save(comment);
    }

    /**
     * Get Sorted Comments from Memory Structure
     * Build/Update the Tree, Lazily
     *
     * @param sortType
     * @return
     */
    @Override
    public List<Comment> getSortedComemnts(SortType sortType) {

        List<Comment> clist = new ArrayList<Comment>();

        // Get Root nodes
        List<CommentNode> rootComments = CommentTree.getInstance().getRootLevelComments(sortType);

        log.debug("rootComments " + rootComments.size());

        if (rootComments.size() == 0) {
            // build tree, lazily at request time
            CommentTree.getInstance().buildSortTree(getCommentsFromDB(sortType), sortType);
        }

        // Get all children of particular sort order
        for (CommentNode commentNode : rootComments) {
            clist.addAll(commentNode.getParentChilds(sortType));
        }

        return clist;

    }


}

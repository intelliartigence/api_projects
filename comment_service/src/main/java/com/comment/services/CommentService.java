package com.comment.services;

import com.comment.model.Comment;
import com.comment.utils.SortType;

import java.util.List;

/**
 * Comment Service Interface
 */
public interface CommentService {

    List<Comment> getCommentsFromDB(SortType sort);

    Comment saveComment(Comment comment);

    List<Comment> getSortedComemnts(SortType sortType);

}

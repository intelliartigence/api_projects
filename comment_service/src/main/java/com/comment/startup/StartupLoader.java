package com.comment.startup;

import com.comment.model.Comment;
import com.comment.services.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;


/**
 * Class to load some data in the H2 Database at startup
 */
@Component
public class StartupLoader implements ApplicationListener<ContextRefreshedEvent> {

    private CommentService CommentService;

    // Generate Random Data?
    Boolean GENERATE_RANDOM_COMMENTS = Boolean.FALSE;

    @Autowired
    public void setCommentService(CommentService CommentService) {
        this.CommentService = CommentService;
    }

    private Logger log = LoggerFactory.getLogger(StartupLoader.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (GENERATE_RANDOM_COMMENTS) {
            Random r = new Random();

            // Load some parent Comments
            for (char c = 'a'; c <= 'z'; c++) {
                Comment comment = new Comment();
                comment.setComment(String.valueOf(c));
                comment.setRating(Long.valueOf(r.nextInt(100)));
                comment.setTimeStamp(System.currentTimeMillis());

                log.info("Creating Comment" + comment);
                CommentService.saveComment(comment);

                try {
                    // Some Delay to get the time sorting to work
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    log.info("Interrupted ");
                }

            }

        } else {

            List<Comment> commentList = Arrays.asList(
                    new Comment(1, 0, 1, 5, "Comment1"),
                    new Comment(2, 0, 2, 10, "Comment1"),
                    new Comment(3, 0, 3, 1, "Comment1"),
                    new Comment(4, 1, 4, 3, "Comment1"),
                    new Comment(5, 4, 5, 5, "Comment1"),
                    new Comment(6, 2, 6, 6, "Comment1"),
                    new Comment(7, 2, 7, 5, "Comment1"),
                    new Comment(8, 7, 8, 5, "Comment1"),
                    new Comment(9, 6, 9, 5, "Comment1"),
                    new Comment(10, 5, 10, 5, "Comment1"),
                    new Comment(11, 10, 11, 5, "Comment1"),
                    new Comment(12, 11, 12, 5, "Comment1"),
                    new Comment(13, 11, 13, 5, "Comment1"),
                    new Comment(14, 3, 14, 5, "Comment1"),
                    new Comment(15, 1, 15, 15, "Comment1"),
                    new Comment(16, 4, 16, 5, "Comment1")
            );

            for (Comment comment : commentList) {
                CommentService.saveComment(comment);
            }
        }
    }
}

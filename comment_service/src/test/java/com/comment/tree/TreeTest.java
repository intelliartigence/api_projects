package com.cmment.tree;

import com.comment.model.Comment;
import com.comment.services.tree.CommentNode;
import com.comment.services.tree.CommentTree;
import com.comment.utils.SortType;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit Test to build and test the output of Tree against Given Rating order
 */
public class TreeTest {


    // Correct order of comment id's if sorting by ratings
    List<Integer> RATINGS_ORDER = Arrays.asList(2, 6, 9, 7, 8, 1, 15, 4, 5, 10, 11, 12, 13, 16, 3, 14);

    private Logger log = LoggerFactory.getLogger(TreeTest.class);

    @Test
    public void testTreeOutput() throws Exception {

        List<Comment> result = new ArrayList<Comment>();
        SortType sortType = SortType.RATING;

        // Build Tree for Ratings Sort
        CommentTree.getInstance().buildSortTree(getCommentsFromDBMock(sortType), sortType);

        // Get Ordered Nodes
        List<CommentNode> rootComments = CommentTree.getInstance().getRootLevelComments(sortType);
        for (CommentNode commentNode : rootComments) {
            result.addAll(commentNode.getParentChilds(sortType));
        }

        // Check against Golden order
        for (int i = 0; i < result.size(); i++) {

            Comment comment = result.get(i);
            Integer matchId = RATINGS_ORDER.get(i);

            log.info("CommentId: " + comment.getCommentId() + " Golden Id: " + matchId);
            assertEquals(comment.getCommentId(), new Long(matchId));

        }

    }


    public List<Comment> getCommentsFromDBMock(SortType sorttype) {

        List<Comment> commentList = Arrays.asList(
                new Comment(1, 0, 1, 5, "Comment1"),
                new Comment(2, 0, 2, 10, "Comment1"),
                new Comment(3, 0, 3, 1, "Comment1"),
                new Comment(4, 1, 4, 3, "Comment1"),
                new Comment(5, 4, 5, 5, "Comment1"),
                new Comment(6, 2, 6, 6, "Comment1"),
                new Comment(7, 2, 7, 5, "Comment1"),
                new Comment(8, 7, 8, 5, "Comment1"),
                new Comment(9, 6, 9, 5, "Comment1"),
                new Comment(10, 5, 10, 5, "Comment1"),
                new Comment(11, 10, 11, 5, "Comment1"),
                new Comment(12, 11, 12, 5, "Comment1"),
                new Comment(13, 11, 13, 5, "Comment1"),
                new Comment(14, 3, 14, 5, "Comment1"),
                new Comment(15, 1, 15, 15, "Comment1"),
                new Comment(16, 4, 16, 5, "Comment1")
        );

        return commentList;

    }
}
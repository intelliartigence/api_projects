
## Hierarchical Comments API   


### Run
1. ```cd comment_service```
1. ``` mvn clean package ```
1. ``` java -jar target/comments-service-1.0.jar ```
1. open browser and go to API UI :http://localhost:8001/swagger-ui.html
1. type RATING or DATE in sort box and hit enter OR
1. curl -X GET 'http://localhost:8001/api/comments?sort=RATING' 
1. curl -X GET 'http://localhost:8001/api/comments?sort=DATE'


### Tests
1. ```mvn test``` for basic unit test 
2. ./test.sh

### Features and Design

1. Based on SpringBoot i.e. Embedded-ed Server, Packaged jar
1. Startup code to populate random comments
1. End-Point for Getting Comments with GET http://localhost:8001/comments
1. Check API tests in ```./test.sh``` 
1. Algorithm and Data Structure for Sorting in-memory
1. In memory H2 Database, to populate the db with some input comments


### Swagger UI
http://localhost:8001/swagger-ui.html

### Tests
1. See ```com.cmment.tree.TreeTest``` for unit tests 

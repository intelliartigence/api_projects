#!/bin/sh


# Get Listing with sort
printf "\n\n ***** Sort by Rating ***** \n"
curl -X GET 'http://localhost:8001/api/comments?sort=RATING'

printf  "\n\n ***** Sort by Date ***** \n"
curl -X GET 'http://localhost:8001/api/comments?sort=DATE'

printf  "\n\n ***** Apache benchmark ***** \n"
ab -c 10 -n 1000 http://localhost:8001/api/comments?sort=RATING
